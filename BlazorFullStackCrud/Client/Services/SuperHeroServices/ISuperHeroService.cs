﻿namespace BlazorFullStackCrud.Client.Services.SuperHeroServices
{
	public interface ISuperHeroService
	{
		List<SuperHero> Heroes { get; set; }
		List<Comic> Comics { get; set; }
		Task GetComics();
		Task GetSuperHeroes();
		Task<SuperHero> GetSingleHero(int id);
		Task CreateSuperHero(SuperHero hero);
		Task UpdateSuperHero(SuperHero hero);
		Task DeleteHero(int id);
	}
}
